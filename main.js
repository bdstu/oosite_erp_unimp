
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import * as Api from 'config/api.js'
import * as Common from 'config/common.js'
import * as Db from 'config/db.js'
import * as Config from 'config/config.js'

Vue.config.productionTip = false
Vue.prototype.$api = Api
Vue.prototype.$common = Common
Vue.prototype.$db = Db
Vue.prototype.$config = Config


App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif